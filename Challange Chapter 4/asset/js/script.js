const pemainBatu = document.getElementById ("pb")
const pemainGunting = document.getElementById ("pg")
const pemainKertas = document.getElementById ("pk")
const computerBatu = document.getElementById ("cb")
const computerGunting = document.getElementById ("cg")
const computerKeartas = document.getElementById ("ck")
const hasil = document.querySelector(".hasil")
const reload = document.getElementById("reload")

function getPilihanComputer(){
    const pilihan = ['cb', 'cg', 'ck']
    const pilihanAcak = Math.floor(Math.random()*3)
    const pilihanComputer = document.getElementById (pilihan[pilihanAcak])
    pilihanComputer.className = "pilcom"
    return pilihan [pilihanAcak]
}

function win() {
    hasil.innerHTML = "Player 1 Win"
    hasil.style.backgroundColor = '#4C9654'
    hasil.style.textAlign = 'center'
    hasil.style.paddingTop = '32px'
    hasil.style.fontSize = '36px'
    hasil.style.height = '130px'
    hasil.style.color = 'white'
    hasil.style.transform = 'rotate(-30deg)'
}

function lose(pemain, computer) {
    hasil.innerHTML = "Computer Win"
    hasil.style.backgroundColor = '#4C9654'
    hasil.style.textAlign = 'center'
    hasil.style.paddingTop = '32px'
    hasil.style.fontSize = '36px'
    hasil.style.height = '130px'
    hasil.style.color = 'white'
    hasil.style.transform = 'rotate(-30deg)'
}

function draw(pemain, computer) {
    hasil.innerHTML = "Draw"
    hasil.style.backgroundColor = '#4C9654'
    hasil.style.textAlign = 'center'
    hasil.style.paddingTop = '32px'
    hasil.style.fontSize = '36px'
    hasil.style.height = '130px'
    hasil.style.color = 'white'
    hasil.style.transform = 'rotate(-30deg)'
}

function game(pilihanPemain){
    const pilihanComputer = getPilihanComputer();
    switch (pilihanPemain + pilihanComputer){
        case "pbcg":
        case "pkcb":
        case "pgck":
            win(pilihanPemain, pilihanComputer);
            break;
        case "pbck":
        case "pkcg":
        case "pgcb":
            lose(pilihanPemain, pilihanComputer);
            break;
        case "pbcb":
        case "pgcg":
        case "pkck":
            draw(pilihanPemain, pilihanComputer);
            break;
    }
}

function main (){
pemainBatu.addEventListener('click', function() {
    game("pb")
})
pemainGunting.addEventListener('click', function() {
    game("pg")
})
pemainKertas.addEventListener('click', function() {
    game("pk")
})
reload.addEventListener('click', function(){
    location.reload()
})
}
main ()
